﻿using System;

namespace SortStone
{
    /// <summary>
    /// Куча камней.
    /// </summary>
    public class HeapStone
    {
        #region Поля и свойства

        /// <summary>
        /// Количество камней.
        /// </summary>
        public int countStoune { get; set; }
        /// <summary>
        /// Массив весов камней.
        /// </summary>
        public int[] weightStone { get; set; }
        /// <summary>
        /// Разность весов 2 куч.
        /// </summary>
        public int differenceMassHeap { get;set;}

        #endregion

        #region Методы

        /// <summary>
        /// Вычисление наименьшей разности.
        /// </summary>
        /// <param name="countStone">Количесвто камней.</param>
        /// <param name="weightStone">Массив весов камней.</param>
        /// <returns>Наименьшую разность куч камней.</returns>
        private void SplitStone(int countStone, int[] weightStone)
        {
            var sumHeap1 = weightStone[0];
            var sumHeap2 = weightStone[1];
            for(var i = 2; i<countStone; i++)
            {
                if (sumHeap1 > sumHeap2)
                {
                    sumHeap2 += weightStone[i];
                }
                else
                {
                    sumHeap1 += weightStone[i];
                }
            }
            differenceMassHeap = Math.Abs(sumHeap1 - sumHeap2);
        }

        /// <summary>
        /// Сортировка массива весов.
        /// </summary>
        /// <param name="weightStone">Массив весов камней.</param>
        /// <returns>Массив весов по убыванию.</returns>
        private int[] SortStone(int[] weightStone)
        {
            Array.Sort(weightStone);
            Array.Reverse(weightStone);
#if DEBUG
            foreach(var element in weightStone)
            {
                Console.Write(element + " ");
            }
            Console.WriteLine();
#endif
            return weightStone;
        }

        /// <summary>
        /// Метод вычисления наименьшей разности куч.
        /// </summary>
        public void CalculateDiffrenceHeap()
        {
            SortStone(this.weightStone);
            SplitStone(this.countStoune, this.weightStone);
        }

        #endregion

        #region Конструкторы

        /// <summary>
        /// Конструктор класса HeapStone.
        /// </summary>
        /// <param name="countStoune">Количество камней.</param>
        /// <param name="weightStone">Массив весов камней.</param>
        public HeapStone(int countStoune, int[] weightStone)
        {
            this.countStoune = countStoune;
            this.weightStone = weightStone;
        }

        #endregion
    }
}
