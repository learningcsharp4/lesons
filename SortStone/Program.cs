﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace SortStone
{
    /// <summary>
    /// Основной класс программы.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Основная программа.
        /// </summary>
        static void Main()
        {
            Console.WriteLine("Введите количество камней от 1 до 20.");
            var input = Console.ReadLine();
            int.TryParse(input, out int countStoune);
            if (countStoune < 1 || countStoune > 20) 
            { 
                Console.WriteLine("Введено неправильное количество камней."); 
                Console.ReadKey();
                return; 
            }
            Console.WriteLine("Введите массу каждого камня от 1 до 100000 через пробел.");
            input = Console.ReadLine();
            var weightStone = new List<int>();
            var weights = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);            
            foreach (var weight in weights)
            {
                int stoune;
                int.TryParse(weight, out stoune);
                if( stoune < 1 || stoune > 100000)
                {
                    Console.WriteLine("Введена неправильнаям масса камней.");
                    Console.ReadKey();
                    return;
                }
                weightStone.Add(stoune);                
            }
            if (weightStone.Count != countStoune)
            {
                Console.WriteLine("Количество введеных весов камней не соответствует количеству камней.");
                Console.ReadKey();
                return;
            }

            HeapStone heapStone1 = new HeapStone(countStoune, weightStone.ToArray());
            heapStone1.CalculateDiffrenceHeap();

            Console.WriteLine(heapStone1.differenceMassHeap);
            Console.ReadKey();
        }
    }
}
